<?php
include 'koneksi.php';
$plant = $_POST['plant'];

$queryResult = $connect->query("SELECT
	a.Nik,
	c.avatar AS Avatar,
	b.lg_name AS Nama,
	MONTH ( recorded_for_date ) AS Bulan,
	a.last_synchronize,
	a.Steps,
	a.Distance,
	d.org_locn_work_code AS org_locn_work_code,
CASE
		
		WHEN d.org_locn_work_code = 'WL001' THEN
		'Head Office AIO' 
		WHEN d.org_locn_work_code IN ( 'WL002', 'WL005', 'WL020', 'WL114', 'WL115' ) THEN
		'ODI' 
		WHEN d.org_locn_work_code IN ( 'WL003', 'WL004' ) THEN
		'Factory' ELSE 'Area' 
	END AS location 
FROM
	v_steps_permonth a
	LEFT JOIN aio_employee.php_ms_login b ON a.nik = b.lg_nik
	LEFT JOIN mst_user_auth c ON a.Nik = c.nik
	LEFT JOIN aio_employee.mst_employment d ON a.nik = RIGHT (
		d.employee_code,
	CHAR_LENGTH( a.nik )) 
WHERE
	Bulan = MONTH (
	NOW())
	AND d.is_active = 1
	AND (
		( d.org_locn_work_code = 'WL001' AND 'Head Office AIO' = '" . $plant . "' ) 
		OR ( d.org_locn_work_code IN ( 'WL002', 'WL005', 'WL020', 'WL114', 'WL115' ) AND 'ODI' = '" . $plant . "' ) 
		OR ( d.org_locn_work_code IN ( 'WL003', 'WL004' ) AND 'Factory' = '" . $plant . "' ) 
		OR (
			'Area' = '" . $plant . "' 
		AND d.org_locn_work_code NOT IN ( 'WL001', 'WL002', 'WL003', 'WL004', 'WL005', 'WL020', 'WL114', 'WL115' )) 
	) 
	AND YEAR ( a.last_synchronize ) = YEAR (
	NOW()) 
	AND b.lg_aktif = '1'
GROUP BY
	a.Nik 
ORDER BY
	Steps DESC;");
$result = array();
while ($fetchData = $queryResult->fetch_assoc()) {
	$result[] = $fetchData;
}
echo json_encode($result);
