
<?php
include 'koneksi.php';
$plant = $_POST['plant'];

function getWeekRange()
{
	// Mendapatkan tanggal hari ini
	$today = new DateTime();

	// Menentukan hari Senin di minggu berjalan
	$monday = clone $today;
	$monday->modify('this week monday');

	// Menentukan hari Minggu di minggu berjalan
	$sunday = clone $today;
	$sunday->modify('this week sunday');

	// Format tanggal menjadi string
	$mondayString = $monday->format('Y-m-d');
	$sundayString = $sunday->format('Y-m-d');

	return [
		'monday' => $mondayString,
		'sunday' => $sundayString
	];
}

$weekRange = getWeekRange();

$queryResult = $connect->query("SELECT
	a.nik AS Nik,
	c.avatar AS Avatar,
	b.lg_name AS Nama,
	a.last_synchronize,
	SUM( a.total_distance ) AS Distance,
	SUM( a.steps ) AS Steps,
	d.org_locn_work_code AS org_locn_work_code,
CASE
		
		WHEN d.org_locn_work_code = 'WL001' THEN
		'Head Office AIO' 
		WHEN d.org_locn_work_code IN ( 'WL002', 'WL005', 'WL020', 'WL114', 'WL115' ) THEN
		'ODI' 
		WHEN d.org_locn_work_code IN ( 'WL003', 'WL004' ) THEN
		'Factory' ELSE 'Area' 
	END AS location,
	a.recorded_for_date 
FROM
	daily_activity_summary a
	LEFT JOIN aio_employee.php_ms_login b ON a.nik = b.lg_nik
	LEFT JOIN mst_user_auth c ON a.nik = c.nik
	LEFT JOIN aio_employee.mst_employment d ON a.nik = RIGHT (
		d.employee_code,
	CHAR_LENGTH( a.nik )) 
WHERE
	DATE ( a.recorded_for_date ) BETWEEN '" . $weekRange['monday'] . "' 
	AND '" . $weekRange['sunday'] . "'
	AND d.is_active = 1
	AND b.lg_aktif = '1' 
	AND (
		( d.org_locn_work_code = 'WL001' AND 'Head Office AIO' = '" . $plant . "' ) 
		OR ( d.org_locn_work_code IN ( 'WL002', 'WL005', 'WL020', 'WL114', 'WL115' ) AND 'ODI' = '" . $plant . "' ) 
		OR ( d.org_locn_work_code IN ( 'WL003', 'WL004' ) AND 'Factory' = '" . $plant . "' ) 
		OR (
			'Area' = '" . $plant . "' 
		AND d.org_locn_work_code NOT IN ( 'WL001', 'WL002', 'WL003', 'WL004', 'WL005', 'WL020', 'WL114', 'WL115' )) 
	) 
GROUP BY
	a.nik 
ORDER BY
	Steps DESC;");

$result = array();
while ($fetchData = $queryResult->fetch_assoc()) {
	$result[] = $fetchData;
}
echo json_encode($result);
?>