<?php
include 'koneksi.php';
$plant = $_POST['plant'];

$queryResult = $connect->query("SELECT
	a.nik AS Nik,
	c.avatar AS Avatar,
	b.lg_name AS Nama,
	a.steps AS Steps,
	a.total_distance AS Distance,
	a.recorded_for_date,
	d.org_locn_work_code AS org_locn_work_code,
CASE
		
		WHEN d.org_locn_work_code = 'WL001' THEN
		'Head Office AIO' 
		WHEN d.org_locn_work_code IN ( 'WL002', 'WL005', 'WL020', 'WL114', 'WL115' ) THEN
		'ODI' 
		WHEN d.org_locn_work_code IN ( 'WL003', 'WL004' ) THEN
		'Factory' ELSE 'Area' 
	END AS location,
	a.last_synchronize 
FROM
	daily_activity_summary a
	LEFT JOIN aio_employee.php_ms_login b ON a.nik = b.lg_nik
	LEFT JOIN mst_user_auth c ON a.nik = c.nik
	LEFT JOIN aio_employee.mst_employment d ON a.nik = RIGHT (
		d.employee_code,
	CHAR_LENGTH( a.nik )) 
WHERE
	DATE ( recorded_for_date ) = DATE (
	NOW())
	AND d.is_active = 1
	AND (
		( d.org_locn_work_code = 'WL001' AND 'Head Office AIO' = '" . $plant . "' ) 
		OR ( d.org_locn_work_code IN ( 'WL002', 'WL005', 'WL020', 'WL114', 'WL115' ) AND 'ODI' = '" . $plant . "' ) 
		OR ( d.org_locn_work_code IN ( 'WL003', 'WL004' ) AND 'Factory' = '" . $plant . "' ) 
		OR (
			'Area' = '" . $plant . "' 
		AND d.org_locn_work_code NOT IN ( 'WL001', 'WL002', 'WL003', 'WL004', 'WL005', 'WL020', 'WL114', 'WL115' )) 
	) 
	AND b.lg_aktif = '1' 
GROUP BY
	a.nik 
ORDER BY
	CAST( a.Steps AS UNSIGNED ) DESC;");
$result = array();
while ($fetchData = $queryResult->fetch_assoc()) {
	$result[] = $fetchData;
}
echo json_encode($result);
