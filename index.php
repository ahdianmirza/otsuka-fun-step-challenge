<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Leader Board Otsuka Active Challenge</title>
    <link rel="icon" type="image/x-icon" href="img/unnamed.webp" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
    h1 {
        font-size: 2rem;
        line-height: 1rem;
        position: absolute;
        margin-left: 1rem;
        top: 4rem;
    }
    </style>
    <link rel="stylesheet" href="css/style.css">
    <script src="js/jquery.min.js"></script>
    <!--google font-->
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800;900&display=swap"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@300;400;600;700&display=swap"
        rel="stylesheet">
</head>

<body>
    <div class="leaderboard">
        <header>
            <h1 class="animated infinite bounce">Leader Board</h1>
            <h3>Otsuka Active Challenge</h3>
            <img class="steps img-def" src="img/jalan_santuy.png" alt="">
            <nav>
                <a href="#" id="ho" onclick="selectPlant('Head Office AIO')">HO</a>
                <a href="#" id="odi" onclick="selectPlant('ODI')">ODI</a>
                <a href="#" id="area" onclick="selectPlant('Area')">Area</a>
                <a href="#" id="factory" onclick="selectPlant('Factory')">Factory</a>
            </nav>
        </header>


        <div class="lboard_wrap">
            <div class="categ">
                <div class="category">
                    <button href="#" id="today" onclick="selectCategory('today')" class="btn">Today</button>
                    <button href="#" id="weekly" onclick="selectCategory('weekly')" class="btn">Weekly</button>
                    <button href="#" id="monthly" onclick="selectCategory('monthly')" class="btn">Monthly</button>

                </div>
            </div>

            <div class="lboard_item month" id="board_data">
                <div class="lboard_mem">
                    <div class="img">
                        <img class="profil"
                            src="https://lh3.googleusercontent.com/a-/AOh14GiAhnSXdv035ENVJB4zMmGiYfSWjIgrPzFzBRegNA=s96-c"
                            alt="picture_2">
                    </div>
                    <div class="name_bar">
                        <p>1. Robi Kurniawan</p>
                        <div class="bar_wrap">
                            <div class="inner_bar" style="width: 95%"></div>
                        </div>
                        <p style="font-weight: 150; font-size: 13px; margin-top: 0.2rem;">11 minutes ago</p>
                    </div>
                    <div class="points">
                        1195 steps
                    </div>
                </div>
                <hr>

            </div>
        </div>
    </div>



    <script src="js/scripts.js"></script>
    <script>
    let plant, category, user, dataplant, datacategory, monthlyLeaderBoard, weeklyLeaderBoard, dailyLeaderBoard;
    let targetDaily = 0,
        targetWeekly = 0,
        targetMonthly = 0;
    let myPosition = [];

    $(document).ready(function() {

        console.log("ready!");
        dataplant = localStorage.getItem("plant");
        datacategory = localStorage.getItem("category");
        user = localStorage.getItem("aio-portal-user");
        console.log("user localStorage: ", user);
        if (dataplant != null && datacategory != null) {
            plant = dataplant;
            category = datacategory;
            selectPlantHead(plant);
            selectCategoryHead(category)
        } else {
            plant = 'Head Office AIO';
            category = 'monthly';
            $("#monthly").addClass("active-btn");
            $("#ho").addClass("active");
        }

        getTarget();
        getData();
        checkSize();


        // getMyPosition();
    });

    function getMyPosition(Obj, Position) {
        let dataHandler = $("#board_data");

        if (Obj.length > 0) {
            if (category == 'monthly') {
                if (monthlyLeaderBoard != null || monthlyLeaderBoard != undefined) {
                    let isInTopTen = monthlyLeaderBoard.filter(function(item) {
                        return item.Nik == Obj[0].nik && item.location == Obj[0].location
                    })

                    if (isInTopTen.length == 0) {
                        let persentase = Number(Obj[0].achivement);
                        let classPersent = '';
                        let classInner = '';

                        if (persentase >= 70) {
                            classPersent = 'bar_wrap';
                            classInner = 'inner_bar';
                        } else if (persentase < 70 && persentase >= 40) {
                            classPersent = 'bar_wrap-yellow';
                            classInner = 'inner_bar-yellow';
                        } else {
                            classPersent = 'bar_wrap-red';
                            classInner = 'inner_bar-red';
                        }

                        let avatar = Obj[0].Avatar != '' ? Obj[0].Avatar : 'img/unnamed.webp';
                        let InnerElement = `
							<h4 style="color:#252424;font-weight:bold; margin-top: 8px;">your position :</h4>

							<div class="lboard_mem">
								<div class="img" >
								<img class="profil" src="${avatar}" alt="aio">
								</div>
								<div class="name_bar">
									<p >${Position}.  ${toTitleCase(Obj[0].Nama)}</p>
									<div class="${classPersent}">
										<div class="${classInner}" style="width: ${persentase}%"></div>
									</div>
									<p style="font-weight: 150; font-size: 13px; margin-top: 0.2rem;">${getDifferentDate(Obj[0].last_synchronize)}</p>
								</div>
								<div class="points">
								${formatNumber(Obj[0].Steps)} Steps
								</div>
							</div>
							`;

                        dataHandler.append(InnerElement)
                    }
                }
            } else if (category == 'weekly') {
                if (weeklyLeaderBoard != null || weeklyLeaderBoard != undefined) {
                    let isInTopTen = weeklyLeaderBoard.filter(function(item) {
                        return item.Nik == Obj[0].nik && item.location == Obj[0].location
                    })

                    if (isInTopTen.length == 0) {
                        let persentase = Number(Obj[0].achivement);
                        let classPersent = '';
                        let classInner = '';

                        if (persentase >= 70) {
                            classPersent = 'bar_wrap';
                            classInner = 'inner_bar';
                        } else if (persentase < 70 && persentase >= 40) {
                            classPersent = 'bar_wrap-yellow';
                            classInner = 'inner_bar-yellow';
                        } else {
                            classPersent = 'bar_wrap-red';
                            classInner = 'inner_bar-red';
                        }

                        let avatar = Obj[0].Avatar != '' ? Obj[0].Avatar : 'img/unnamed.webp';
                        let InnerElement = `
							<h4 style="color:#252424;font-weight:bold; margin-top: 8px;">your position :</h4>

							<div class="lboard_mem">
								<div class="img" >
								<img class="profil" src="${avatar}" alt="aio">
								</div>
								<div class="name_bar">
									<p >${Position}.  ${toTitleCase(Obj[0].Nama)}</p>
									<div class="${classPersent}">
										<div class="${classInner}" style="width: ${persentase}%"></div>
									</div>
									<p style="font-weight: 150; font-size: 13px; margin-top: 0.2rem;">${getDifferentDate(Obj[0].last_synchronize)}</p>
								</div>
								<div class="points">
								${formatNumber(Obj[0].Steps)} Steps
								</div>
							</div>
							`;

                        dataHandler.append(InnerElement)
                    }
                }
            } else if (category == 'today') {
                if (dailyLeaderBoard != null || dailyLeaderBoard != undefined) {
                    let isInTopTen = dailyLeaderBoard.filter(function(item) {
                        return item.Nik == Obj[0].nik && item.location == Obj[0].location
                    })

                    if (isInTopTen.length == 0) {
                        let persentase = Number(Obj[0].achivement);
                        let classPersent = '';
                        let classInner = '';

                        if (persentase >= 70) {
                            classPersent = 'bar_wrap';
                            classInner = 'inner_bar';
                        } else if (persentase < 70 && persentase >= 40) {
                            classPersent = 'bar_wrap-yellow';
                            classInner = 'inner_bar-yellow';
                        } else {
                            classPersent = 'bar_wrap-red';
                            classInner = 'inner_bar-red';
                        }

                        let avatar = Obj[0].Avatar != '' ? Obj[0].Avatar : 'img/unnamed.webp';
                        let InnerElement = `
							<h4 style="color:#252424;font-weight:bold; margin-top: 8px;">your position :</h4>

							<div class="lboard_mem">
								<div class="img" >
								<img class="profil" src="${avatar}" alt="aio">
								</div>
								<div class="name_bar">
									<p >${Position}.  ${toTitleCase(Obj[0].Nama)}</p>
									<div class="${classPersent}">
										<div class="${classInner}" style="width: ${persentase}%"></div>
									</div>
									<p style="font-weight: 150; font-size: 13px; margin-top: 0.2rem;">${getDifferentDate(Obj[0].last_synchronize)}</p>
								</div>
								<div class="points">
								${formatNumber(Obj[0].Steps)} Steps
								</div>
							</div>
							`;

                        dataHandler.append(InnerElement)
                    }
                }
            }
        }
    }

    function formatNumber(numbers) {
        return new Intl.NumberFormat("id-ID").format(numbers);
    }

    function checkSize() {
        if ($(window).width() < 343) {
            $("#myacc").text("Achievement");
        }
    }

    function selectPlantHead(data) {
        if (data == 'Head Office AIO') {
            $("#ho").addClass("active");
            $("#odi").removeClass("active");
            $("#area").removeClass("active");
            $("#factory").removeClass("active");
            plant = 'Head Office AIO'
            localStorage.setItem("plant", "Head Office AIO");
        } else if (data == 'Factory') {
            $("#ho").removeClass("active");
            $("#odi").removeClass("active");
            $("#area").removeClass("active");
            $("#factory").addClass("active");
            plant = 'Factory'
            localStorage.setItem("plant", "Factory");
        } else if (data == 'ODI') {
            $("#ho").removeClass("active");
            $("#odi").addClass("active");
            $("#area").removeClass("active");
            $("#factory").removeClass("active");
            plant = 'ODI'
            localStorage.setItem("plant", "ODI");
        } else if (data == 'Area') {
            $("#ho").removeClass("active");
            $("#odi").removeClass("active");
            $("#area").addClass("active");
            $("#factory").removeClass("active");
            plant = 'Area';
            localStorage.setItem("plant", "Area");
        }

        // getData();
    }

    function gotoApp() {
        let username = localStorage.getItem("aio-portal-user");
        let password = localStorage.getItem("uid");

        console.log(password);
        if (username != undefined && password != undefined) {
            link = `https://myapps.aio.co.id/leader-board/admin/login?username=${username}&password=${password}`
            window.open(link, '_self')
        } else {
            window.open("https://myapps.aio.co.id/leader-board/admin/dashboard2", '_self')
        }
    }

    function selectPlant(data) {
        if (data == 'Head Office AIO') {
            $("#ho").addClass("active");
            $("#odi").removeClass("active");
            $("#area").removeClass("active");
            $("#factory").removeClass("active");
            plant = 'Head Office AIO'
            localStorage.setItem("plant", "Head Office AIO");
        } else if (data == 'Factory') {
            $("#ho").removeClass("active");
            $("#odi").removeClass("active");
            $("#area").removeClass("active");
            $("#factory").addClass("active");
            plant = 'Factory'
            localStorage.setItem("plant", "Factory");
        } else if (data == 'ODI') {
            $("#ho").removeClass("active");
            $("#odi").addClass("active");
            $("#area").removeClass("active");
            $("#factory").removeClass("active");
            plant = 'ODI'
            localStorage.setItem("plant", "ODI");
        } else if (data == 'Area') {
            $("#ho").removeClass("active");
            $("#odi").removeClass("active");
            $("#area").addClass("active");
            $("#factory").removeClass("active");
            plant = 'Area';
            localStorage.setItem("plant", "Area");
        }

        getData();
    }

    function selectCategory(data) {
        localStorage.setItem("category", data);
        if (data == 'today') {
            $("#today").addClass("active-btn");
            $("#monthly").removeClass("active-btn");
            $("#weekly").removeClass("active-btn");
        } else if (data == 'monthly') {
            $("#monthly").addClass("active-btn");
            $("#today").removeClass("active-btn");
            $("#weekly").removeClass("active-btn");
        } else {
            $("#monthly").removeClass("active-btn");
            $("#today").removeClass("active-btn");
            $("#weekly").addClass("active-btn");
        }
        category = data;
        getData();
    }

    function selectCategoryHead(data) {
        localStorage.setItem("category", data);
        if (data == 'today') {
            $("#today").addClass("active-btn");
            $("#monthly").removeClass("active-btn");
            $("#weekly").removeClass("active-btn");
        } else if (data == 'monthly') {
            $("#monthly").addClass("active-btn");
            $("#today").removeClass("active-btn");
            $("#weekly").removeClass("active-btn");
        } else {
            $("#monthly").removeClass("active-btn");
            $("#today").removeClass("active-btn");
            $("#weekly").addClass("active-btn");
        }
        category = data;
    }

    function getTarget() {
        $.ajax({
            type: 'POST',
            url: 'getDataTarget.php',
            success: function(result) {
                var objResult = JSON.parse(result);
                targetDaily = Number(objResult[0].daily_target)
                targetWeekly = Number(objResult[0].weekly_target)
                targetMonthly = Number(objResult[0].monthly_target)
                // if(objResult.length == 0) {
                // }
            }
        })
    }

    function getData() {
        $("#board_data").empty();
        if (category == 'monthly') {
            var dataHandler = $("#board_data");
            weeklyLeaderBoard = null;
            dailyLeaderBoard = null;
            $("#board_data").empty();

            $.ajax({
                type: 'POST',
                data: "plant=" + plant,
                url: 'getDataMonthly.php',
                success: function(result) {
                    var objResult = JSON.parse(result);
                    monthlyLeaderBoard = objResult;
                    $("#board_data").empty();

                    if (objResult.length == 0) {
                        $("#board_data").empty();
                        let kosong = `
						
						<div>
						<br><br><br><br><br>
						<center><h4 class="kosong">Data Kosong</h4></center>
						</div>`

                        dataHandler.append(kosong);
                    }

                    $.each(objResult, function(key, val) {
                        let persentase = (Number(val.Steps) / targetMonthly) * 100;
                        let classPersent = '';
                        let classInner = '';

                        if (persentase >= 70) {
                            classPersent = 'bar_wrap';
                            classInner = 'inner_bar';
                        } else if (persentase < 70 && persentase >= 40) {
                            classPersent = 'bar_wrap-yellow';
                            classInner = 'inner_bar-yellow';
                        } else {
                            classPersent = 'bar_wrap-red';
                            classInner = 'inner_bar-red';
                        }

                        let avatar = val.Avatar != '' ? val.Avatar : 'img/unnamed.webp';

                        if (key == 0) {
                            let InnerElement = `
							<div class="lboard_mem">
								<div class="img" >
								    <img class="crown" style="width:25px;" src="img/Crown-gold.png">
								    <img class="profil" src="${avatar}" alt="aio">
								</div>

								<div class="name_bar">
									<p >${key+1}.  ${toTitleCase(val.Nama)}</p>

									<div class="${classPersent}">
										<div class="${classInner}" style="width: ${persentase}%"></div>
									</div>

									<p style="font-weight: 150; font-size: 13px; margin-top: 0.2rem;">${getDifferentDate(val.last_synchronize)}</p>
								</div>

								<div class="points">
								    ${formatNumber(val.Steps)} Steps
								</div>
							</div>
							<hr>`;
                            dataHandler.append(InnerElement);
                        } else if (key == 1) {
                            let InnerElement = `
							<div class="lboard_mem">
								<div class="img" >
								    <img class="crown" style="width:25px;" src="img/Crown-silver.png">
								    <img class="profil" src="${avatar}" alt="aio">
								</div>

								<div class="name_bar">
									<p>${key+1}.  ${toTitleCase(val.Nama)}</p>

									<div class="${classPersent}">
										<div class="${classInner}" style="width: ${persentase}%"></div>
									</div>

									<p style="font-weight: 150; font-size: 13px; margin-top: 0.2rem;">${getDifferentDate(val.last_synchronize)}</p>
								</div>

								<div class="points">
								    ${formatNumber(val.Steps)} Steps
								</div>
							</div>
							<hr>`;
                            dataHandler.append(InnerElement);
                        } else if (key == 2) {
                            let InnerElement = `
							<div class="lboard_mem">
								<div class="img" >
								    <img class="crown" style="width:25px;" src="img/Crown-perunggu.png">
								    <img class="profil" src="${avatar}" alt="aio">
								</div>

								<div class="name_bar">
									<p>${key+1}.  ${toTitleCase(val.Nama)}</p>

									<div class="${classPersent}">
										<div class="${classInner}" style="width: ${persentase}%"></div>
									</div>

									<p style="font-weight: 150; font-size: 13px; margin-top: 0.2rem;">${getDifferentDate(val.last_synchronize)}</p>
								</div>

								<div class="points">
								    ${formatNumber(val.Steps)} Steps
								</div>
							</div>
							<hr>`;
                            dataHandler.append(InnerElement);
                        } else {
                            let InnerElement = `
							<div class="lboard_mem">
								<div class="img" >
								    <img class="profil" src="${avatar}" alt="aio">
								</div>

								<div class="name_bar">
									<p>${key+1}.  ${toTitleCase(val.Nama)}</p>

									<div class="${classPersent}">
										<div class="${classInner}" style="width: ${persentase}%"></div>
									</div>

									<p style="font-weight: 150; font-size: 13px; margin-top: 0.2rem;">${getDifferentDate(val.last_synchronize)}</p>
								</div>

								<div class="points">
								    ${formatNumber(val.Steps)} Steps
								</div>
							</div>
							<hr>`;
                            dataHandler.append(InnerElement);
                        }

                        if (key == 9) {
                            console.log('true')
                            return false;
                            console.log('false')
                        }
                    })

                    //  console.log(objResult)

                    if (user != null) {
                        let MyData = objResult.filter(function(el) {
                            return el.Nik == user
                        });
                        const myposisi = (element) => element.Nik == user;
                        let posisiku = objResult.findIndex(myposisi) + 1;
                        console.info("posisiku: ", posisiku);
                        getMyPosition(MyData, posisiku)
                    }
                }
            })
        } else if (category == 'today') {
            var dataHandler = $("#board_data");
            monthlyLeaderBoard = null;
            weeklyLeaderBoard = null;
            $("#board_data").empty();

            $.ajax({
                type: 'POST',
                data: "plant=" + plant,
                url: 'getDataToday.php',
                success: function(result) {
                    var objResult = JSON.parse(result);
                    dailyLeaderBoard = objResult;
                    $("#board_data").empty();

                    if (objResult.length == 0) {
                        $("#board_data").empty();
                        let kosong = `
						<div>
						<br><br><br><br><br>
						<center><h4 class="kosong">Data Kosong</h4></center>
						</div>`

                        dataHandler.append(kosong);
                    }

                    $.each(objResult, function(key, val) {
                        let persentase = (Number(val.Distance) / targetDaily) * 100;
                        let classPersent = '';
                        let classInner = '';

                        if (persentase >= 70) {
                            classPersent = 'bar_wrap';
                            classInner = 'inner_bar';
                        } else if (persentase < 70 && persentase >= 40) {
                            classPersent = 'bar_wrap-yellow';
                            classInner = 'inner_bar-yellow';
                        } else {
                            classPersent = 'bar_wrap-red';
                            classInner = 'inner_bar-red';
                        }

                        let avatar = val.Avatar != '' ? val.Avatar : 'img/unnamed.webp'

                        if (key == 0) {
                            let InnerElement = `
							<div class="lboard_mem">
								<div class="img" >
								    <img class="crown" style="width:25px;" src="img/Crown-gold.png">
								    <img class="profil" src="${avatar}" alt="aio">
								</div>

								<div class="name_bar">
									<p>${key+1}.  ${toTitleCase(val.Nama)}</p>

								    <div class="${classPersent}">
										<div class="${classInner}" style="width: ${persentase}%"></div>
									</div>

									<p style="font-weight: 150; font-size: 13px; margin-top: 0.2rem;">${getDifferentDate(val.last_synchronize)}</p>
								</div>

								<div class="points">
								    ${formatNumber(val.Steps)} Steps
								</div>
							</div>
							<hr>
							`;
                            dataHandler.append(InnerElement);
                        } else if (key == 1) {
                            let InnerElement = `
							<div class="lboard_mem">
								<div class="img" >
								    <img class="crown" style="width:25px;" src="img/Crown-silver.png">
								    <img class="profil" src="${avatar}" alt="aio">
								</div>

								<div class="name_bar">
									<p >${key+1}.  ${toTitleCase(val.Nama)}</p>

								    <div class="${classPersent}">
										<div class="${classInner}" style="width: ${persentase}%"></div>
									</div>
                                    
									<p style="font-weight: 150; font-size: 13px; margin-top: 0.2rem;">${getDifferentDate(val.last_synchronize)}</p>
								</div>

								<div class="points">
								    ${formatNumber(val.Steps)} Steps
								</div>
							</div>
							<hr>
							`;
                            dataHandler.append(InnerElement);
                        } else if (key == 2) {
                            let InnerElement = `
							<div class="lboard_mem">
								<div class="img" >
								    <img class="crown" style="width:25px;" src="img/Crown-perunggu.png">
								    <img class="profil" src="${avatar}" alt="aio">
								</div>

								<div class="name_bar">
									<p>${key+1}.  ${toTitleCase(val.Nama)}</p>

								    <div class="${classPersent}">
										<div class="${classInner}" style="width: ${persentase}%"></div>
									</div>

									<p style="font-weight: 150; font-size: 13px; margin-top: 0.2rem;">${getDifferentDate(val.last_synchronize)}</p>
								</div>

								<div class="points">
								    ${formatNumber(val.Steps)} Steps
								</div>
							</div>
							<hr>
							`;
                            dataHandler.append(InnerElement);
                        } else {
                            let InnerElement = `
							<div class="lboard_mem">
								<div class="img">
								    <img class="profil" src="${avatar}" alt="aio">
								</div>

								<div class="name_bar">
									<p>${key+1}.  ${toTitleCase(val.Nama)}</p>

								    <div class="${classPersent}">
										<div class="${classInner}" style="width: ${persentase}%"></div>
									</div>

									<p style="font-weight: 150; font-size: 13px; margin-top: 0.2rem;">${getDifferentDate(val.last_synchronize)}</p>
								</div>

								<div class="points">
								    ${formatNumber(val.Steps)} Steps
								</div>
							</div>
							<hr>
							`;
                            dataHandler.append(InnerElement);
                        }

                        if (key == 9) {
                            console.log('true')
                            return false;
                            console.log('false')
                        }
                    })

                    if (user != null) {
                        let MyData = objResult.filter(function(el) {
                            return el.Nik == user
                        });
                        const myposisi = (element) => element.Nik == user;
                        let posisiku = objResult.findIndex(myposisi) + 1;
                        console.info("posisiku: ", posisiku);
                        getMyPosition(MyData, posisiku)
                    }
                }
            })
        } else if (category == 'weekly') {
            var dataHandler = $("#board_data");
            monthlyLeaderBoard = null;
            dailyLeaderBoard = null;
            $("#board_data").empty();

            $.ajax({
                type: 'POST',
                data: "plant=" + plant,
                url: 'getDataWeekly.php',
                success: function(result) {
                    var objResult = JSON.parse(result);
                    weeklyLeaderBoard = objResult;

                    if (objResult.length == 0) {
                        $("#board_data").empty();
                        let kosong = `
						
						<div>
						<br><br><br><br><br>
						<center><h4 class="kosong">Data Kosong</h4></center>
						</div>`
                        dataHandler.append(kosong);
                    }

                    $.each(objResult, function(key, val) {
                        let persentase = (Number(val.Distance) / targetWeekly) * 100;
                        let classPersent = '';
                        let classInner = '';

                        if (persentase >= 70) {
                            classPersent = 'bar_wrap';
                            classInner = 'inner_bar';
                        } else if (persentase < 70 && persentase >= 40) {
                            classPersent = 'bar_wrap-yellow';
                            classInner = 'inner_bar-yellow';
                        } else {
                            classPersent = 'bar_wrap-red';
                            classInner = 'inner_bar-red';
                        }

                        let avatar = val.Avatar != '' ? val.Avatar : 'img/unnamed.webp'

                        if (key == 0) {
                            let InnerElement = `
							<div class="lboard_mem">
								<div class="img" >
								    <img class="crown" style="width:25px;" src="img/Crown-gold.png">
								    <img class="profil" src="${avatar}" alt="aio">
								</div>

								<div class="name_bar">
									<p>${key+1}.  ${toTitleCase(val.Nama)}</p>

								    <div class="${classPersent}">
										<div class="${classInner}" style="width: ${persentase}%"></div>
									</div>

									<p style="font-weight: 150; font-size: 13px; margin-top: 0.2rem;">${getDifferentDate(val.last_synchronize)}</p>
								</div>

								<div class="points">
								    ${formatNumber(val.Steps)} Steps
								</div>
							</div>
							<hr>
							`;
                            dataHandler.append(InnerElement);
                        } else if (key == 1) {
                            let InnerElement = `
							<div class="lboard_mem">
								<div class="img" >
								    <img class="crown" style="width:25px;" src="img/Crown-silver.png">
								    <img class="profil" src="${avatar}" alt="aio">
								</div>

								<div class="name_bar">
									<p>${key+1}.  ${toTitleCase(val.Nama)}</p>

								    <div class="${classPersent}">
										<div class="${classInner}" style="width: ${persentase}%"></div>
									</div>

									<p style="font-weight: 150; font-size: 13px; margin-top: 0.2rem;">${getDifferentDate(val.last_synchronize)}</p>
								</div>

								<div class="points">
								    ${formatNumber(val.Steps)} Steps
								</div>
							</div>
							<hr>
							`;
                            dataHandler.append(InnerElement);
                        } else if (key == 2) {
                            let InnerElement = `
							<div class="lboard_mem">
								<div class="img">
								    <img class="crown" style="width:25px;" src="img/Crown-perunggu.png">
								    <img class="profil" src="${avatar}" alt="aio">
								</div>

								<div class="name_bar">
									<p>${key+1}.  ${toTitleCase(val.Nama)}</p>

								    <div class="${classPersent}">
										<div class="${classInner}" style="width: ${persentase}%"></div>
									</div>

									<p style="font-weight: 150; font-size: 13px; margin-top: 0.2rem;">${getDifferentDate(val.last_synchronize)}</p>
								</div>

								<div class="points">
								    ${formatNumber(val.Steps)} Steps
								</div>
							</div>
							<hr>
							`;
                            dataHandler.append(InnerElement);
                        } else {
                            let InnerElement = `
							<div class="lboard_mem">
								<div class="img">
								    <img class="profil" src="${avatar}" alt="aio">
								</div>

								<div class="name_bar">
									<p>${key+1}.  ${toTitleCase(val.Nama)}</p>
                                    
								    <div class="${classPersent}">
									    <div class="${classInner}" style="width: ${persentase}%"></div>
									</div>

									<p style="font-weight: 150; font-size: 13px; margin-top: 0.2rem;">${getDifferentDate(val.last_synchronize)}</p>
								</div>

								<div class="points">
								    ${formatNumber(val.Steps)} Steps
								</div>
							</div>
							<hr>
							`;
                            dataHandler.append(InnerElement);
                        }

                        if (key == 9) {
                            console.log('true')
                            return false;
                            console.log('false')
                        }
                    })

                    if (user != null) {
                        let MyData = objResult.filter(function(el) {
                            return el.Nik == user
                        });
                        const myposisi = (element) => element.Nik == user;
                        let posisiku = objResult.findIndex(myposisi) + 1;
                        console.info("posisiku: ", posisiku);
                        getMyPosition(MyData, posisiku)
                    }
                }
            })
        }
    }


    function toTitleCase(str) {
        return str.replace(
            /\w\S*/g,
            function(txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            }
        );
    }

    function kiloMeterConvert(jarak) {
        return (Number(jarak) / 1000).toFixed(2) + ' km'
    }


    function getDifferentDate(date) {
        let date1_ms = new Date(formatDateAndTime(date)).getTime();
        let date2_ms = new Date(HariIni()).getTime();
        // Calculate the difference in milliseconds
        let difference_ms = date2_ms - date1_ms;
        //take out milliseconds
        difference_ms = difference_ms / 1000;

        difference_ms = difference_ms / 60;
        let minutes = Math.floor(difference_ms % 60);
        difference_ms = difference_ms / 60;
        let hours = Math.floor(difference_ms % 24);
        let days = Math.floor(difference_ms / 24);

        let hari = '';
        let jam = '';
        let menit = '';
        if (days > 0) {
            hari = days + ' days ago'

            return hari
        } else {
            if (hours > 0) {
                jam = hours + ' hours ago'
                return jam
            } else {
                if (minutes > 0) {
                    menit = minutes + ' minutes ago'
                    return menit
                } else {
                    return '-'
                }
            }
        }
    }

    function formatDateAndTime(date) {
        if (date != '' && date != null && date != undefined) {
            return date
        } else {
            return '-';
        }
    }

    function HariIni() {
        let today = new Date();
        let date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
        let time = today.getHours() + ':' + today.getMinutes() + ':' + today.getSeconds();
        return date + ' ' + time;
    }
    </script>

</body>

</html>